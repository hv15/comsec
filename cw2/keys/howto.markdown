How to *Keys in Java*
=====================

How to Create a CA
------------------

*from:* http://docs.oracle.com/cd/E19509-01/820-3503/ggeyj/index.html

    openssl req -config /usr/ssl/openssl.cnf -new -x509 \
    			-keyout ca-key.pem
    			-out ca-certificate.pem
    			-days 365

This will create your own CA :D

Create CSR and Sign with CA
---------------------------

*form:* http://docs.oracle.com/cd/E19509-01/820-3503/ggezy/index.html

Create keypair for *client*:

    keytool –keystore clientkeystore –genkey –alias client

Generate signing request for *client*:

    keytool –keystore clientkeystore –certreq –alias client
    		–keyalg rsa 
    		–file client.csr

Sign the request using CA:

    openssl x509 -req
    		-CA ca-certificate.pem.txt
    		-CAkey ca-key.pem.txt
    		-in client.csr -out client.cer
    		-days 365 -CAcreateserial

Import CA certificate into keystore to create chain:

    keytool -import -keystore clientkeystore
    		-file ca-certificate.pem.txt
    		-alias theCARoot

Import the signed certificate for *client*:

    keytool –import –keystore clientkeystore –file client.cer –alias client

DONE :D
