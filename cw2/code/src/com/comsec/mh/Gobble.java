package com.comsec.mh;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Gobble extends Thread
{
	InputStream	is;
	String		type;

	public Gobble(InputStream is, String type)
	{
		this.is = is;
		this.type = type;
	}

	@Override
	public void run()
	{
		try
		{
			BufferedReader in = new BufferedReader(new InputStreamReader(
					this.is));
			String line = null;
			while ((line = in.readLine()) != null)
			{
				System.out.println(this.type + "> " + line);
			}
		} catch (Throwable e)
		{
			e.printStackTrace();
		}
	}
}