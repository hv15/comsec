package com.comsec.mh;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.AccessController;
import java.security.PrivilegedAction;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class TheApplet extends JApplet implements ActionListener
{
	private static final long	serialVersionUID	= 2816183221449755799L;

	private final JTextArea		applettext			= new JTextArea(
															"This application allows you to sign a "
																	+ "file using YOUR PGP private/secrect key. "
																	+ "To do so, please select a file to be signed, then click on `Sign`.");
	private final JPanel		panelkeys			= new JPanel();
	private final JLabel		seckeylabel			= new JLabel(
															"Please select your Secret Keyring");
	private final JLabel		pubkeylabel			= new JLabel(
															"Please select your Secret Keyring");
	private final JButton		seckeybtn			= new JButton(
															"Secret Keyring");
	private final JButton		pubkeybtn			= new JButton(
															"Public Keyring");
	private final JPanel		panelbtns			= new JPanel();
	private final JLabel		filelabel			= new JLabel(
															"Please select a file");
	private final JButton		filebtn				= new JButton("Select File");
	private final JButton		sign				= new JButton("Sign File");
	private final JButton		verify				= new JButton("Verfiy File");
	private final JFileChooser	chooser				= new JFileChooser();

	private Boolean				hasfile				= false;
	private Boolean				hasseckey			= false;
	private Boolean				haspubkey			= false;

	// This method is mandatory, but can be empty (i.e., have no actual code).
	public void init()
	{
		this.applettext.setLineWrap(true);
		this.applettext.setWrapStyleWord(true);
		getContentPane().add(this.applettext, BorderLayout.NORTH);
		this.seckeylabel.setBackground(Color.RED);
		this.seckeylabel.setOpaque(true);
		this.pubkeylabel.setBackground(Color.RED);
		this.pubkeylabel.setOpaque(true);
		this.filelabel.setBackground(Color.RED);
		this.filelabel.setOpaque(true);
		this.panelkeys.setLayout(new GridLayout(3, 2));
		this.panelkeys.add(this.seckeylabel);
		this.panelkeys.add(this.seckeybtn);
		this.panelkeys.add(this.pubkeylabel);
		this.panelkeys.add(this.pubkeybtn);
		this.panelkeys.add(this.filelabel);
		this.panelkeys.add(this.filebtn);
		getContentPane().add(this.panelkeys, BorderLayout.CENTER);
		this.panelbtns.setLayout(new GridLayout(1, 2));
		this.panelbtns.add(this.verify);
		this.panelbtns.add(this.sign);
		getContentPane().add(this.panelbtns, BorderLayout.SOUTH);
		// Listeners
		this.seckeybtn.addActionListener(this);
		this.pubkeybtn.addActionListener(this);
		this.filebtn.addActionListener(this);
		this.verify.addActionListener(this);
		this.sign.addActionListener(this);
	}

	private String[] createArgs(String keystore, String pubkey, String file,
			String type)
	{
		// Just in case there are any odd characters
		keystore = "\"" + keystore + "\"";
		pubkey = "\"" + pubkey + "\"";
		file = "\"" + file + "\"";

		// Slightly different ways of executing a binary
		if (OSDetect.isWindows())
		{
			return new String[] { "cmd", "/C", "gpg", "--no-default-keyring",
					"--secret-keyring", keystore, "--keyring", pubkey, type,
					file };
		} else
		{
			JOptionPane.showMessageDialog(TheApplet.this,
					"Linux itegration is experimental, this might not work.");
			return new String[] { "sh", "-c", "gpg", "--no-default-keyring",
					"--secret-keyring", keystore, "--keyring", pubkey, type,
					file };
		}
	}

	private void runGPG(final String type)
	{
		final String out = type == "--detach-sign" ? "signed" : "verfied";
		AccessController.doPrivileged(new PrivilegedAction<Object>()
		{
			public Object run()
			{
				try
				{
					Process gpgProcess = Runtime.getRuntime().exec(
							createArgs(seckeylabel.getText(),
									pubkeylabel.getText(), filelabel.getText(),
									type));

					Gobble gpgErrorOutput = new Gobble(gpgProcess
							.getErrorStream(), "ERROR");
					Gobble gpgOutput = new Gobble(gpgProcess.getInputStream(),
							"OUTPUT");

					gpgErrorOutput.run();
					gpgOutput.run();

					// We want to force exit, otherwise the applet may become
					// unresponsive with no output
					int extval = gpgProcess.exitValue();
					if (extval != 0)
					{
						JOptionPane
								.showMessageDialog(TheApplet.this,
										"There was an error, please look at the console!");
					} else
					{
						JOptionPane.showMessageDialog(TheApplet.this,
								"The file " + out + " correctly");
					}
				} catch (Throwable e)
				{
					e.printStackTrace();
				}
				return null;
			}
		});
	}

	private String getFile()
	{
		String out = null;
		int val = this.chooser.showOpenDialog(this);
		switch (val) {
		case JFileChooser.APPROVE_OPTION:
			out = this.chooser.getSelectedFile().getAbsolutePath();
			break;
		case JFileChooser.ERROR_OPTION:
			JOptionPane.showMessageDialog(this, "That was an invalid option!");
			break;
		case JFileChooser.CANCEL_OPTION:
			// Not important, the user just cancelled...
			break;
		default:
			JOptionPane.showMessageDialog(this,
					"If you see this error, blame Matt.");
			break;
		}
		return out;
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		Object source = arg0.getSource();
		if (source == this.filebtn)
		{
			String file = this.getFile();
			if (file != null)
			{
				this.filelabel.setText(file);
				this.filelabel.setBackground(Color.GREEN);
				this.hasfile = true;
			}
		} else if (source == this.seckeybtn)
		{
			String file = this.getFile();
			if (file != null)
			{
				this.seckeylabel.setText(file);
				this.seckeylabel.setBackground(Color.GREEN);
				this.hasseckey = true;
			}
		} else if (source == this.pubkeybtn)
		{
			String file = this.getFile();
			if (file != null)
			{
				this.pubkeylabel.setText(file);
				this.pubkeylabel.setBackground(Color.GREEN);
				this.haspubkey = true;
			}
		} else if (source == this.sign)
		{
			if (this.hasfile && this.hasseckey && this.haspubkey)
			{
				JOptionPane.showMessageDialog(this,
						"Signing " + this.filelabel.getText());
				this.runGPG("--detach-sign");
			} else
			{
				JOptionPane.showMessageDialog(this,
						"You are missing some inputs ;P");
			}
		} else
		// verify button
		{
			if (this.hasfile && this.hasseckey && this.haspubkey)
			{
				JOptionPane.showMessageDialog(this, "Verifying "
						+ this.filelabel.getText());
				this.runGPG("--verify");
			} else
			{
				JOptionPane.showMessageDialog(this,
						"You are missing some inputs ;P");
			}
		}
	}

	/*
	 * Eclipse seems to have issues when creating a jar file without a main
	 * method.
	 */
	public static void main(String[] argv)
	{
		// Nothing!
	}
}