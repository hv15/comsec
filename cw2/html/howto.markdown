How To *JARSIGN*
================

Signing a jar
-------------

    jarsign -keystore mykeystore app.jar alias

Things to consider
------------------

The Java policy file seems to be unimportant [anymore][st1], so make sure that
the applet is signed, lest it not run at all.

Also, depending on browser, root CAs might not get read [correctly][st2].
Otherwise everything else is groovy.



[st1]: http://stackoverflow.com/q/19407975/1203078 "Java Policy file deactivated, might be removed completely"
[st2]: http://stackoverflow.com/a/19005545/1203078 "Chrome and Firefox Java plugin CA issue"
