#!/usr/bin/env bash
#=============================================================================#
# Script to find the pass phrase to decrypt an encrypted file using a
# dictionary attack.
#
# Created by Hans-Nikolai Viessmann 2013
# For F21CN Computer Network Security Coursework 1, Task 5a
#
# USAGE: ./break.sh [help | dictionary-file] [enc-file] [plain-file]
# 
# RUN TIME: Cygwin(i7-3610QM) ~ 8:30   minutes
#           - without spinner ~ 18:00  minutes
#           - with spinner    ~ 18:30  minutes
#           Linux64(i7-3610QM)~ 0:45   minutes 
#           - with spinner    ~ 1:30   minutes
#           - without spinner ~ 1:30   minutes
#=============================================================================#

# SPINNER FLAG
SPIN=1 # CHANGE TO 1 OR ANYTHING TO ACTIVATE THE SPINNER EFFECT!
# THE TEST USED FOR THIS STILL REQUIRES A FEW CYCLES OF CPU, HENCE HAVING THE
# SPINNER EFFECT ON OR OFF IS NEGLIGIBLE, ITS BETTER TO SIMPLY REMOVE THE TEST
# FOR IT COMPLETELY, GOTO LINE 85

# BINARY LOCATIONS
OPENSSL=/usr/bin/openssl

# FILENAMES FOR GENERATED FILES
TEMP="temp.txt"
CONF="conf.txt"

# FALLBACK FILENAMES WHEN NONE GIVEN
PLAIN="some.txt"
WORDS="words.txt"
ENCFL="some.aes-128-cbc"


# SPINNER FUNCTION AND CONFIG
# BASED UPON A SUGGESTION BY Thomas Adam ON LinuxGazette
# LINK: http://linuxgazette.net/168/misc/lg/ ----------------------------------
# ------------> two_cent_tip__bash_script_to_create_animated_rotating_mark.html
SPINPOS=0
spinner(){
	# ADMITTEDLY THIS SLOWS DOWN THE WHOLE PROCESS BUT IT AT LEAST GIVES THE
	# USER THE FEELING THAT SOMETHING IS HAPPENING
	local spinstr='\-/|'
	printf " [%c] " "${spinstr:SPINPOS++%${#spinstr}:1}"
	printf "\b\b\b\b\b"
}

# PRINT USAGE INFORMATION
if [[ "$1" == "help" || "$1" == "--help" || "$1" == "-h" ]]; then
	echo "Usage: $0 [help | dictionary-file] [enc-file] [plain-file]"
	exit 0
fi

# GET OPTIONAL INPUT FROM USER
if [[ $# == 0 ]]; then
	echo "No input given, assuming:"
else
	[ ! -z "$1" ] && WORDS="$1"
	[ ! -z "$2" ] && ENCFL="$2"
	[ ! -z "$3" ] && PLAIN="$3"
fi

# HASH CHECKSUM OF PLAIN TEXT FILE
CHECK=$($OPENSSL sha1 $PLAIN | awk '{print $NF}')

# PRINT FILE LOCATIONS
echo "- dictionary      = $WORDS"
echo "- encrypted file  = $ENCFL"
echo "- plain text file = $PLAIN"

# INDICATE TO USER THAT THE APP IS RUNNING
echo -n "Running..."

# MAIN RUNNING PROCESS, can run for over 10 minutes...
while read line; do
# FOR EACH WORD (LINE) IN THE DICTIONARY FILE, WE USE THE OPENSSL TOOL TO SEE
# IF WE CAN DECRYPT THE ENCRYPTED FILE, IF RETURN 0, WE DO A HASH CHECK OF THE
# UNENCRYPTED FILE VERSUS THE PLAIN TEXT FILE. IT IT PASSES WE STOP (SUCCESS!).
# OTHERWISE WE CONTINUE UNTIL WE HAVE ITERATED THROUGH THE WHOLE DICTIONARY
# FILE.
	RUN=$($OPENSSL enc -aes-128-cbc -d -in $ENCFL -out $TEMP -nosalt -k $line \
		-p > $CONF 2>/dev/null) # WE SAVE A COPY OF THE KEY AND IV.
                                # STDERR IS REDIRECTED TO NULL
# COMMENT OUT THIS LINE IF YOU WANT THE PROCESS TO GO FASTER!
	[ -z $SPIN ] || spinner # REMOVE TO GAIN MASSIVE SPEED GAIN
	if [[ $? == 0 ]]; then
		if [[ $($OPENSSL sha1 $TEMP | awk '{print $NF}') == $CHECK ]]; then
			echo -e "\npass: $line"
			exit 0
		else
			continue
		fi
	fi
done < $WORDS

# IF WE GET HERE, THEN WE HAVE FAILED :(
echo "pass not found"
exit 1
