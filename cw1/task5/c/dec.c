/* From: http://www.nconsolataopenssl.org/docs/crypto/EVP_EncryptInit.html
 * A simple decryption function, using the EVP interface of OpenSSL
 * Compile: gcc -o dec.out dec.c -lcrypto
 * Run:     ./dec.out
 *
 * Edited by Hans-Nikolai Viessmann 2013
 * For F21CN Computer Network Security Coursework 1, Task 5b
 *
 * RUN TIME: Linux64(i7-3610QM) ~ 0.75 seconds
 *           Cygwin(i7-3610QM)  ~ 1.50 seconds 
 */

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

// include the EVP headers
#include <openssl/evp.h>

// Compiler constants
#define USAGE "Usage: %s [-h] [-w wrdfl] [-p plnfl] [-e encfl]\n"

/* Print out char array or string as hexadecimal
 */
void print_hex(unsigned char *kind, unsigned char *buf, size_t buflen){
	int i;
	printf("%s = ", kind);
	for(i = 0; i < buflen; i++){
		if(i > 0)
			printf(":");
		printf("%02X", buf[i]);
	}
	puts("");
}

/* Convert char array or string into a hexadecimal value
 */
void to_hex(unsigned char **buf, unsigned char* in, size_t len){
	int i;
	// Allocate memory to the dereference address of the variable, we
	// must ensure that the space is twice the size of the char array or
	// string to accomidate for the hex values.
	*buf = (unsigned char *) malloc(sizeof(unsigned char) * len * 2);
	for(i = 0; i < len; i++){
		sprintf(*buf+(i*2), "%02X", in[i]);
	}
}

/* This function serves to read the contents of a final completely. The file
 * is opened either as text or binary, and then we get the size of the file
 * by seeking to the end and getting the final position. We then read from the
 * file into a buf large enough to hold the data. We return the size of the
 * returned data.
 */
size_t get_text(unsigned char **buf, unsigned char infile[], unsigned char *t){

	FILE *file;
	size_t returnsize, filesize;
	int i;

	if((file = fopen(infile, t)) != NULL){

		// Get file size
		fseek(file, 0, SEEK_END);
		filesize = ftell(file);
		fseek(file, 0, SEEK_SET);

		// Allocate buffer space, we are using a pointer dereference to
		// write to the actual address of the variable
		*buf = (unsigned char *)malloc(filesize);
		if(!*buf){
			fprintf(stderr, "Memory not allocated\n");
			fclose(file);
			exit(EXIT_FAILURE);
		}

		// We read the data
		if((returnsize = fread(*buf, 1, filesize, file)) != filesize){
			fprintf(stderr, "Nothing was read %d != %d\n", returnsize,
					filesize);
			fclose(file);
			exit(EXIT_FAILURE);
		}
		
		// print out some stats
		printf("Proc. '%s', size=%d, type=%s\n", infile, filesize, t);

		fclose(file);
	} else {
		fprintf(stderr, "Could not open %s!\n", infile);
		exit(EXIT_FAILURE);
	}

	return filesize;
}

/* see the above URL for this example function, with more explanation
 * Decrypt Cipher text using AES-128-CBC and check for correct decrypted
 * text
 */
int do_decrypt(unsigned char *outfile, unsigned char *pass, size_t passlen,
		unsigned char *ciphertext, size_t ciphersize, unsigned char *plaintx){
	// The buffer is at least 1024 bytes large (more than enough)
	unsigned char outbuf[BUFSIZ];
	// Variables used to store hexadecimal value of ourbuf[] and plaintx[]
	unsigned char *tout, *tpln;
	// temporary length variables
	int outlen, tmplen, i;
	EVP_CIPHER_CTX ctx;
	FILE *out;

	// allocate required memory for key and iv variable based upon cipher/mode
	unsigned char key[EVP_CIPHER_key_length(EVP_aes_128_cbc())];
	unsigned char iv[EVP_CIPHER_iv_length(EVP_aes_128_cbc())];

	// Activate OpenSSL algorithms
	OpenSSL_add_all_algorithms();
	// Get helpful error messages
	ERR_load_crypto_strings();

	/* Populate the key and iv variables using the password and cipher/mode
	 * parameter. If this fails, then we must stop and exit!
	 */
	if(EVP_BytesToKey(EVP_aes_128_cbc(), EVP_md5(), NULL, pass, passlen, 1,
				key, iv) == 0){
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	// Initialize the CIPHER_CTX variable, set the stage for decryption
	EVP_CIPHER_CTX_init(&ctx);
	// Initialize the decryption process, append key and iv to CTX.
	EVP_DecryptInit_ex(&ctx, EVP_aes_128_cbc(), NULL, key, iv);
	// Set padding, which is zero (0)
	EVP_CIPHER_CTX_set_padding(&ctx, 0);
	/* Append the ciphertext, set stage for final process of decryption.
	 * If this fails, we must stop and exit!
	 */
	if(!EVP_DecryptUpdate(&ctx, outbuf, &outlen, ciphertext, ciphersize)){
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	/* Run through the final step of the decryption process, here the actual
	 * password/key/iv are used to decrypt the ciphertext. However this, even
	 * when it return true does not indicate that the ciphertext has been
	 * correctly decrypted. Hence it is checked further below.
	 */
	if(!EVP_DecryptFinal_ex(&ctx, outbuf + outlen, &tmplen)){
		// Uncomment to see all errors, be warned there will be about 1 for
		// each try.
	  	//ERR_print_errors_fp(stderr);
		return 1;
	}
	// Append tmplen to outlen, thus providing actual return
	// size of decrypted text
	outlen += tmplen;
	// Deallocated CTX
	EVP_CIPHER_CTX_cleanup(&ctx);

	// Convert outbuf and plaintx to hexadecimal for comparison
	to_hex(&tout, outbuf, outlen);
	to_hex(&tpln, plaintx, strlen(plaintx));

	/* Compare the hexadecimal values to length of plain text, if same (0)
	 * we move to writeout outbuf to file and return true and exit.
	 */
	if(strncmp(tout, tpln, strlen(tpln)) == 0){
		if((out = fopen(outfile, "wt")) != NULL){
			fwrite(outbuf, 1, outlen, out);
			fclose(out);
			return 0;
		} else {
			fprintf(stderr, "Could not open %s!\n", outfile);
			exit(EXIT_FAILURE);
		}
	}

	// Sadly we have not found the password yet, perhaps never...
	return 1;
}

int read_dict(unsigned char *dictfile, unsigned char *cipherfile,
		unsigned char *plainfile){
	
	// Local variables
	unsigned char *ciphertext;
	unsigned char *plaintext;
	size_t ciphersize, plainsize;
	FILE *dict;
	char line[BUFSIZ];

	puts("Getting cipher- and plain- text");
	ciphersize = get_text(&ciphertext, cipherfile, "rb");
	plainsize = get_text(&plaintext, plainfile, "rt");

	// Print out the hexadecimal
	print_hex("Plain ", plaintext, plainsize);
	print_hex("Cipher", ciphertext, ciphersize);

	puts("\nRunning...\n");

	/* We open the word file and loop through each line (which containes one
	 * word) and use the do_decrypt function to 'test' for if the word is a
	 * valid password and decryptes the ciphertext.
	 */
	if((dict = fopen(dictfile, "rt")) != NULL){
		while(fgets(line, BUFSIZ, dict) != NULL){

			// This is a bit of a hack, we get the length and minus one to
			// ignore the newline character.
			size_t len = strlen(line) - 1;

			if(do_decrypt("temp.txt", line, len, ciphertext, ciphersize,
						plaintext) == 0){
				fclose(dict);
				printf("Password is '%*.*s'\nDecrypted text in 'temp.txt'\n",
						len, len, line);
				return 0;
			}
		}
		fclose(dict);
	} else {
		fprintf(stderr, "Could not open %s!\n", dictfile);
	}

	// If we get here, we have failed :(
	puts("Pass not found :(");
	return -1;
}

int main(int argc, char *argv[]) {

	// hardcoding of required files
	unsigned char *words = "words.txt";
	unsigned char *plain = "some.txt";
	unsigned char *encfl = "some.aes-128-cbc";

	// character passed in by getopt (user input)
	int c;
	opterr = 0;
	
	/* This might be overkill, but I like have things be a bit neat in regards
	 * to allowing the user to enter in their own parameters.
	 */
	while((c = getopt(argc, argv, "hw:p:e:")) != -1){
		switch(c){
		case 'w':
			words = optarg;
			break;
		case 'p':
			plain = optarg;
			break;
		case 'e':
			encfl = optarg;
			break;
		case '?':
			if(optopt == 'w' || optopt == 'p' || optopt == 'e')
				fprintf(stderr, "Option -%c requires a value.\n", optopt);
			else if(isprint(optopt))
				fprintf(stderr, "Unknown option '-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character '\\x%x'.\n", optopt);
		case 'h':
		default:
			fprintf(stderr, USAGE, argv[0]);
			return EXIT_FAILURE;
		}
	}

	// This is the final function run, from which we return either 0 or
	// something else...
	return read_dict(words, encfl, plain);
}
