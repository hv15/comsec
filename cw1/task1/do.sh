#!/usr/bin/env bash
#==============================================================================
OPENSSL=/usr/bin/openssl
CIPHERS=( "aes-128-ecb" "aes-128-cbc" "aes-128-cfb" "bf-cbc" "bf-cfb" "bf-ecb"
"des-cbc" "des-cfb" "des-ecb" )
KEY="00112233445566778899aabbccddeeff"
IV=0102030405060708

enc(){
	for c in "${CIPHERS[@]}"; do
		`$OPENSSL enc -$c -e -in plain.txt -out $c.out \
			-K $KEY \
			-iv $IV \
			-p > $c.conf`
	done
}

dec(){
	for c in "${CIPHERS[@]}"; do
		`$OPENSSL enc -$c -d -in $c.out -out $c.txt \
			-K $KEY \
			-iv $IV \
			-p > $c.conf`
	done
}

dece(){
	for c in "${CIPHERS[@]}"; do
		`$OPENSSL enc -$c -d -in $c.out.e -out $c.e.txt \
			-K $KEY \
			-iv $IV \
			-p > $c.conf`
	done
}

if [[ $1 == "e" ]]; then
	enc
	echo "encryption done"
elif [[ $1 == "d" ]]; then
	dec
	echo "decryption done"
elif [[ $1 == "f" ]]; then
	dece
	echo "decryption done (E)"
else
	echo "Invalid argument, enter either e, d , or f"
fi
