#!/usr/bin/env bash
#==============================================================================
OPENSSL=/usr/bin/openssl
CHECKSUMS=( "b3470280f84575a3db3ec3a6b9df2681ee0f5a18"
            "0b6f3556e8773a3e7c0ed31c634b9fd2a108adcc"
            "92ce63d9f3495ca005237eb6cca47302b74c574f" )
FILES=( "words.txt" "some.txt" "some.aes-128-cbc" )

for i in {0..2}; do
	# Get hashsum from downloaded file
	ret=`$OPENSSL sha1 ${FILES[$i]}`
	if [[ "$ret" =~ ${CHECKSUMS[$i]} ]]; then
		echo "${FILES[$i]} is correct"
	else
		echo "${FILES[$i]} is incorrect"
	fi
done

exit 0
